### Description:

This section is a high level overview of what you plan on refactoring.

### Acceptance Criteria:

- [ ] All unit tests are passing.
- [ ] Print statements have been removed.
- [ ] Doc strings have been added or updated where necessary.

### Definition of Done:

- [ ] Documentation (if needed)
- [ ] Approved and completed merge request(s)

### Testing Instructions:

This section should contain the instructions for running the tests written for this issue.

### Notes:

This section can include articles and other resources that might help the developer.
