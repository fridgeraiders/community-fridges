6/28/23

6/29/23

Today we setup the database and pgAdmin as team. We made a merge request for that work and verified it worked on all of our local machines.

6/30/23

Today I "drove" as we finished backend Auth and submitted a merge request. We then got started on our individual backend endpoints. I made a lot of progress on the Items endpoints and prepopulating values with items.

7/3/23 - 7/7/23

Summer break, read some of the past learns again.

7/10/2023

I finished up the Item endpoints and put in a merge request to main. I got started on the User endpoints.

7/11/2023

I worked on the user endpoints. The user update endpoint and some error handling took me longer than I anticipated.

7/12/2023

I refactored the user endpoints a bit and put in a merge request to main of the finished user endpoints.

7/13/23

Made some decisions today as to what tailwind component libraries we'd use and memorialized some of those decisions on the wiki. Updated the openapi meta data information for many of our endpoints and changed up some of the user endpoint routes more.

7/14/23

Added some new issue templates to our project and made some more formatting / styling updates to our open api docs.

7/17/23

I installed Tailwind CSS into the React app. As a team we worked on creating the auth forms for signup and login on the front end, tested it together. Carmen worked on signup and Maria worked on login.

7/18/23

We all started working on the rest of our front end componenents / screens. I worked on the fridge list screen, but also ended up refactoring the fridge endpoints quite a bit. This included implementing a join to pass a nested neighborhood object within the out model for fridge endpoints.

7/19/23
Spent a good amount of time on the add a fridge and fridge list screens. Implemented some helper functions so that the time inputs on add a fridge was more intuitive. Also worked on the responsiveness of the fridge list page.

7/20/23
I spent a good amount of time on the fridge detail page styling and error handling for inactive fridges and a 404 page. Also worked on fridge list screen search and logged in filtering to include inactive fridges in the list. Started to implement an additional endpoint on fridge checkins to show all checkins for a specific fridge.

7/24/23

Wrapped up the fridge detail page and the last fridge checkin section of that screen.

7/25/23

Worked through deployment today and the associated errors in our tests, builds and deployments.

7/26/23

Presented our MVP to instructors and received feedback. Continued to make styling tweaks.

7/27/23

Worked through updates to fridge checkins list and pantry checkins list for visual consistency and sorting.

7/28/23

Worked on homepage as a group and worked on a mobile navigation midday.
