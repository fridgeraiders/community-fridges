# User Story Using WireFrames

## Home Page

This is the page that a user will first be taken to which is the homepage. On the home page we have a welcoming site where users can gain more access by logging into their account or users can signup to become a volunteer. If a user is there to see a list of fridges or pantries they are able to do this as well.

![Home Page](images/homepagenew.png)

## Signup Page

A user is able to become a volunteer on our website which grants them more functionality like seeing check-in lists and requests and also allowing them to see a list of other volunteers. They are also able to now see inactive fridges or pantries. Volunteers are also able to create new neighborhoods, pantries and fridges.

![Sign Up Page](images/signup.png)

## Login Page

After a user has an account they are able to log in to our website using our log in page.

![Login Page](images/login.png)

## Users List

As a logged in user you now have access to our users list page which shows you other volunteers.

![Login Page](images/userlist.png)

## Pantry List

When you click on pantries from the homepage or in the nav bar, as a logged out user you can view a list of pantries. When you are logged in you are able to see more options on the pantry list page which includes seeing if a pantry is active or inactive. You can also now create a pantry.

![Pantry List](images/pantrylist.png)

## Pantry Form

Now that you are logged in user you are able to create new pantries!

![Pantry Form](images/createpantry.png)

## Pantry Details

When you go back to the pantry list page you are able to click on the actual pantry and be taken to the pantry detail page. If you are not logged in, you still get access to the pantry detail page. The pantry detail page will also filter by the last checkin that was made.

![Pantry Detail](images/pantrydetail.png)

## Pantry Checkin-In Form

When you are on a pantry detail page you are also able to create a pantry checkin which will take to you to a new form. A checkin is used to keep pantries updated as much as possible for volunteers. There, you are able to select your neighborhood, which pantry you are visiting, what brought you to the pantry, the clean status of the pantry, how much food is currently in the pantry, which items were stocked, an updated photo, any additional details and also you can leave your name or initials. It doesn't matter whether you are a logged in user or not, you can still create pantry check-ins.

![Pantry Check-In](images/pantrycheckin.png)

## Pantry Check-In List

Now if you are a logged in user/volunteer, you are able to see a list of all of the pantry check-ins.

![Pantry Check-In List](images/pantrycheckinlist.png)

## Fridge List

When you click on fridges from the homepage or in the nav bar, as a logged out user you can view a list of fridges. When you are logged in, you are able to see more options on the fridge list page which includes seeing if a fridge is active or inactive. You can also now create a fridge.

![Fridge List](images/fridgelist.png)

## Fridge Form

Now that you are logged in user you are able to create new fridges!

![Fridge Form](images/createfridgewireframe.png)

## Fridge Details

When you go back to the fridge list page you are able to click on the actual fridge in the fridge list and are taken to the fridge detail page. If you are not logged in, you still get access to the fridge detail page. The fridge detail page will also filter by the last checkin that was made.

![Fridge Detail](images/fridgedetail.png)

## Fridge Checkin-In Form

When you are on a fridge detail page you are also able to create a fridge checkin which will take to you to a new form. A checkin is used to keep fridges updated as much as possible for volunteers. There, you are able to select your neighborhood, which fridge you are visiting, what brought you to the fridge, the clean status of the fridge, how much food is currently in the fridge, which items were stocked, an updated photo, any additional details and also you can leave your name or initials. It doesn't matter whether you are a logged in user or not, you can still create fridge check-ins.

![Fridge Check-In](images/fridgecheckin.png)

## Fridge Check-In List

Now if you are a logged in user/volunteer, you are able to see a list of all of the fridge check-ins.

![Fridge Check-In List](images/fridgecheckinlist.png)

## Neighborhood List

As a user or volunteer, you are able to look at our neighborhood list which will show you which neighborhoods have pantry and fridges. You will also be able to see the postal codes associated with that neighborhood.

![Neighborhood List](images/neighborhoodlist.png)

## Neighborhood Form

If you are a logged in user you are able to create a new neighborhood and assign a postal code to that neighborhood.

![Neighborhood Form](images/createneighborhoodform.png)
