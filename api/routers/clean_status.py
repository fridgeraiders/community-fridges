from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.clean_status import (
    CleanStatusIn,
    CleanStatusOut,
    CleanStatusRepo,
    Error,
)
from authenticator import authenticator

router = APIRouter()


@router.post(
    "/clean_statuses",
    tags=["Clean Statuses"],
    response_model=Union[CleanStatusOut, Error],
)
def create_clean_status(
    clean_status: CleanStatusIn,
    response: Response,
    repo: CleanStatusRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> CleanStatusOut | Error:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to create a clean status")
    new_status = repo.create(clean_status)
    if new_status is None:
        response.status_code = 400
        return Error(message="Unable to create clean status")
    return new_status


@router.put(
    "/clean_statuses/{clean_status_id}",
    tags=["Clean Statuses"],
    response_model=Union[CleanStatusOut, Error],
)
def update_clean_status(
    clean_status_id: int,
    clean_status: CleanStatusIn,
    response: Response,
    repo: CleanStatusRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[CleanStatusOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to update food status.")
    result = repo.update(clean_status_id, clean_status)
    if result is None:
        response.status_code = 400
    return result


@router.delete(
    "/clean_statuses/{clean_status_id}",
    tags=["Clean Statuses"],
    response_model=bool,
)
def delete_clean_status(
    clean_status_id: int,
    response: Response,
    repo: CleanStatusRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> bool:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to delete clean status.")
    return repo.delete(clean_status_id)


@router.get(
    "/clean_statuses/{clean_status_id}",
    tags=["Clean Statuses"],
)
def get_one_clean_status(
    clean_status_id: int,
    response: Response,
    repo: CleanStatusRepo = Depends(),
) -> CleanStatusOut:
    status = repo.get_one(clean_status_id)
    if status is None:
        response.status_code = 404
    return status


@router.get(
    "/clean_statuses",
    tags=["Clean Statuses"],
    response_model=Union[List[CleanStatusOut], Error],
)
def get_all_clean_statuses(
    repo: CleanStatusRepo = Depends(),
):
    return repo.get_all()
