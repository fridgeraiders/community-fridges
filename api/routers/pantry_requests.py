from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.pantry_requests import (
    Error,
    PantryRequestIn,
    PantryRequestRepository,
    PantryRequestOut,
)


router = APIRouter()


@router.post(
    "/pantry-requests/",
    tags=["Pantry Requests"],
    response_model=Union[PantryRequestOut, Error],
)
def create_a_pantry_request(
    request: PantryRequestIn,
    response: Response,
    repo: PantryRequestRepository = Depends(),
) -> PantryRequestOut | Error:
    result = repo.create(request)
    if result is None:
        response.status_code = 404
        return Error(message="Unable to create pantry request.")
    return result


@router.get(
    "/pantry-requests/",
    tags=["Pantry Requests"],
    response_model=Union[List[PantryRequestOut], Error],
)
def get_all_pantry_requests(
    repo: PantryRequestRepository = Depends(),
):
    return repo.get_all()
