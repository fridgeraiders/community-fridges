from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.items import (
    Error,
    ItemIn,
    ItemRepository,
    ItemOut,
)
from authenticator import authenticator


router = APIRouter()


@router.post("/items/", tags=["Items"], response_model=Union[ItemOut, Error])
def create_a_item(
    item: ItemIn,
    response: Response,
    repo: ItemRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> ItemOut | Error:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to create an item.")
    result = repo.create(item)
    if result is None:
        response.status_code = 404
        result = Error(message="Unable to create item.")
    return result


@router.get(
    "/items/", tags=["Items"], response_model=Union[List[ItemOut], Error]
)
def get_all_items(
    repo: ItemRepository = Depends(),
):
    return repo.get_all()


@router.get("/items/{item_id}/", tags=["Items"])
async def get_one_item(
    item_id: int,
    items: ItemRepository = Depends(),
) -> ItemOut:
    item = items.get_one(item_id)
    return item


@router.put("/items/{item_id}/", tags=["Items"])
async def update_one_item(
    item_id: int,
    item: ItemIn,
    response: Response,
    repo: ItemRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[ItemOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to update an item.")
    result = repo.update(item_id, item)
    if "message" in result:
        response.status_code = 400
    else:
        response.status_code = 200
        return result


@router.delete("/items/{item_id}/", tags=["Items"])
async def delete_one_item(
    item_id: int,
    response: Response,
    repo: ItemRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[ItemOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to delete an item.")
    result = repo.delete(item_id)
    response.status_code = result.get("status", 500)
    if response.status_code != 200:
        response.status_code = 404
        return Error(message=result["message"])
    return result
