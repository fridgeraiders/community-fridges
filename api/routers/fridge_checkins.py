from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.fridge_checkins import (
    FridgeCheckInsIn,
    FridgeCheckInsRepo,
    FridgeCheckInsOut,
    Error,
)
from authenticator import authenticator


router = APIRouter()


@router.get(
    "/api/fridgecheckins/",
    tags=["Fridge Check-Ins"],
    response_model=Union[List[FridgeCheckInsOut], Error],
)
def get_all_checkins(
    response: Response,
    repo: FridgeCheckInsRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to view check-ins.")
    return repo.get_all()


@router.get(
    "/api/fridgecheckins/{id}",
    tags=["Fridge Check-Ins"],
    response_model=Union[List[FridgeCheckInsOut], Error],
)
def get_checkins_for_a_fridges(
    id: int,
    response: Response,
    repo: FridgeCheckInsRepo = Depends(),
) -> FridgeCheckInsOut | Error:
    result = repo.get_all_checkins_for_fridge(id)
    if result is None:
        response.status_code = 404
        result = Error(message="Unable to get checkins")
    return result


@router.post(
    "/api/fridgecheckins",
    tags=["Fridge Check-Ins"],
    response_model=Union[FridgeCheckInsOut, Error],
)
def create_fridge_checkin(
    checkin: FridgeCheckInsIn,
    response: Response,
    repo: FridgeCheckInsRepo = Depends(),
) -> FridgeCheckInsOut | Error:
    result = repo.create(checkin)
    if result is None:
        response.status_code = 404
        result = Error(message="Unable to create item.")
    return result
