from fastapi.testclient import TestClient
from main import app
from queries.fridges import fridgesRepository

client = TestClient(app)


class EmptyFridgesQueries:
    def get_all(self):
        return []

    def test_init():
        assert 1 == 1


def test_get_all_fridges():
    app.dependency_overrides[fridgesRepository] = EmptyFridgesQueries
    response = client.get("/fridges")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []
