from fastapi.testclient import TestClient
from main import app
from queries.pantries import PantryRepository

client = TestClient(app)


class EmptyPantryQueries:
    def get_all(self):
        return []


def test_init():
    assert 1 == 1


def test_get_all_pantries():
    app.dependency_overrides[PantryRepository] = EmptyPantryQueries
    response = client.get("/pantries")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []
