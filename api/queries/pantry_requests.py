from pydantic import BaseModel
from typing import List, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class PantryRequestIn(BaseModel):
    pantry: int
    additional: str
    visitor: str
    items: List[int]


class PantryRequestOut(BaseModel):
    id: int
    pantry: int
    date_created: date
    additional: str
    visitor: str
    items: List[int]


class PantryRequestRepository:
    def create(
        self, request: PantryRequestIn
    ) -> Union[PantryRequestOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO pantry_requests
                            (pantry, additional, visitor)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id, pantry, date_created, additional, visitor;
                        """,
                        [
                            request.pantry,
                            request.additional,
                            request.visitor,
                        ],
                    )
                    record = result.fetchone()

                    for item_id in request.items:
                        db.execute(
                            """
                            INSERT INTO pantry_request_items
                                (pantry_request_id, item_id)
                            VALUES
                                (%s, %s)
                            """,
                            [
                                record[0],
                                item_id,
                            ],
                        )
                    conn.commit()
                    return self.record_to_pantry_request_out(
                        record, request.items
                    )
        except Exception:
            return {"message": "Creating the pantry request did not work."}

    def get_all(self) -> Union[Error, List[PantryRequestOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, pantry, date_created, additional, visitor
                        FROM pantry_requests
                        ORDER BY date_created;
                        """
                    )
                    pantry_requests = []
                    for record in result:
                        with conn.cursor() as db_items:
                            db_items.execute(
                                """
                                SELECT item_id
                                FROM pantry_request_items
                                WHERE pantry_request_id = %s
                                """,
                                [record[0]],
                            )
                            items = [item[0] for item in db_items.fetchall()]
                        pantry_requests.append(
                            self.record_to_pantry_request_out(record, items)
                        )

                    return pantry_requests
        except Exception:
            return {"message": "Could not get all pantry requests"}

    def record_to_pantry_request_out(self, record, items):
        return PantryRequestOut(
            id=record[0],
            pantry=record[1],
            date_created=record[2],
            additional=record[3],
            visitor=record[4],
            items=items,
        )
