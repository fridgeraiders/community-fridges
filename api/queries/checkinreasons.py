from pydantic import BaseModel
from queries.pool import pool
from typing import List, Union, Optional


class Error(BaseModel):
    message: str


class CheckInReasonsIn(BaseModel):
    name: str


class CheckInReasonsOut(BaseModel):
    id: int
    name: str


class ReasonsRepo:
    def get_all(self) -> Union[Error, List[CheckInReasonsOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name
                        FROM check_in_reasons

                        """
                    )
                    return [
                        self.record_to_reason_out(reason) for reason in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all reasons"}

    def create(self, reason: CheckInReasonsIn):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO check_in_reasons
                        (name)
                    VALUES
                        (%s)
                    RETURNING id;
                    """,
                    [reason.name],
                )
                id = result.fetchone()[0]
                return self.reason_in_to_out(id, reason)

    def delete(self, reason_id: int) -> dict:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM check_in_reasons
                        WHERE id = %s
                        """,
                        [reason_id],
                    )
                    if db.rowcount == 0:
                        return {
                            "message": "Could not delete that item because it does not exist.",
                            "status": 404,
                        }
                    conn.commit()
                    return {
                        "message": "Reason successfully deleted.",
                        "status": 200,
                    }
        except Exception:
            return {
                "message": "An unexpected error has occurred.",
                "status": 500,
            }

    def update(
        self, reason_id: int, reason: CheckInReasonsIn
    ) -> Union[CheckInReasonsOut, Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE check_in_reasons
                        SET name = %s
                        WHERE id = %s
                        """,
                        [reason.name, reason_id],
                    )
                    if db.rowcount == 0:
                        return {
                            "message": "Could not update that item because it does not exist.",
                            "status": 404,
                        }
                    conn.commit()
                    return self.reason_in_to_out(reason_id, reason)
        except Exception:
            return {"message": "Could not update that item."}

    def get_one(self, reason_id: int) -> Optional[CheckInReasonsOut]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id
                             , name
                        FROM check_in_reasons
                        WHERE id = %s
                        """,
                        [reason_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_reason_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that item"}

    def reason_in_to_out(self, id: int, reason: CheckInReasonsIn):
        old_data = reason.dict()
        return CheckInReasonsOut(id=id, **old_data)

    def record_to_reason_out(self, record):
        return CheckInReasonsOut(
            id=record[0],
            name=record[1],
        )
