from pydantic import BaseModel
from typing import List, Optional, Union
from queries.pool import pool


class Error(BaseModel):
    message: str


class ItemIn(BaseModel):
    name: str


class ItemOut(BaseModel):
    id: int
    name: str


class ItemRepository:
    def get_one(self, item_id: int) -> Optional[ItemOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                             , name
                        FROM items
                        WHERE id = %s
                        """,
                        [item_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_item_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that item"}

    def delete(self, item_id: int) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM items
                        WHERE id = %s
                        """,
                        [item_id],
                    )
                    if db.rowcount == 0:
                        return {
                            "message": "Could not delete that item because it does not exist.",
                            "status": 404,
                        }
                    conn.commit()
                    return {
                        "message": "Item successfully deleted.",
                        "status": 200,
                    }
        except Exception:
            return {
                "message": "An unexpected error has occurred.",
                "status": 500,
            }

    def update(self, item_id: int, item: ItemIn) -> Union[ItemOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE items
                        SET name = %s
                        WHERE id = %s
                        """,
                        [item.name, item_id],
                    )
                    if db.rowcount == 0:
                        return {
                            "message": "Could not update that item because it does not exist.",
                            "status": 404,
                        }
                    conn.commit()
                    return self.item_in_to_out(item_id, item)
        except Exception:
            return {"message": "Could not update that item."}

    def get_all(self) -> Union[Error, List[ItemOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name
                        FROM items
                        ORDER BY id;
                        """
                    )

                    return [
                        self.record_to_item_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all items"}

    def create(self, item: ItemIn) -> Union[ItemOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO items
                            (name)
                        VALUES
                            (%s)
                        RETURNING id;
                        """,
                        [
                            item.name,
                        ],
                    )
                    id = result.fetchone()[0]

                    return self.item_in_to_out(id, item)
        except Exception:
            return {"message": "Creating the item did not work."}

    def item_in_to_out(self, id: int, item: ItemIn):
        old_data = item.dict()
        return ItemOut(id=id, **old_data)

    def record_to_item_out(self, record):
        return ItemOut(
            id=record[0],
            name=record[1],
        )
