from pydantic import BaseModel
from typing import List, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class FridgeRequestIn(BaseModel):
    fridge: int
    additional: str
    visitor: str
    items: List[int]


class FridgeRequestOut(BaseModel):
    id: int
    fridge: int
    date_created: date
    additional: str
    visitor: str
    items: List[int]


class FridgeRequestRepository:
    def create(
        self, request: FridgeRequestIn
    ) -> Union[FridgeRequestOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO fridge_requests
                            (fridge, additional, visitor)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id, fridge, date_created, additional, visitor;
                        """,
                        [
                            request.fridge,
                            request.additional,
                            request.visitor,
                        ],
                    )
                    record = result.fetchone()

                    for item_id in request.items:
                        db.execute(
                            """
                            INSERT INTO fridge_request_items
                                (fridge_request_id, item_id)
                            VALUES
                                (%s, %s)
                            """,
                            [
                                record[0],
                                item_id,
                            ],
                        )
                    conn.commit()
                    return self.record_to_fridge_request_out(
                        record, request.items
                    )
        except Exception as e:
            print(e)
            return {"message": "Creating the fridge request did not work."}

    def get_all(self) -> Union[Error, List[FridgeRequestOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, fridge, date_created, additional, visitor
                        FROM fridge_requests
                        ORDER BY date_created;
                        """
                    )
                    fridge_requests = []
                    for record in result:
                        with conn.cursor() as db_items:
                            db_items.execute(
                                """
                                SELECT item_id
                                FROM fridge_request_items
                                WHERE fridge_request_id = %s
                                """,
                                [record[0]],
                            )
                            items = [item[0] for item in db_items.fetchall()]
                        fridge_requests.append(
                            self.record_to_fridge_request_out(record, items)
                        )

                    return fridge_requests
        except Exception as e:
            print(e)
            return {"message": "Could not get all fridge requests"}

    def record_to_fridge_request_out(self, record, items):
        return FridgeRequestOut(
            id=record[0],
            fridge=record[1],
            date_created=record[2],
            additional=record[3],
            visitor=record[4],
            items=items,
        )
