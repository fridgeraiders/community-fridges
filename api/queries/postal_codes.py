from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class PostalCodeIn(BaseModel):
    postal_code: str
    neighborhood_id: int


class PostalCodeOut(BaseModel):
    id: int
    postal_code: str
    neighborhood_id: int


class Error(BaseModel):
    message: str


class PostalCodeRepo:
    def code_in_to_out(self, id: int, code: PostalCodeIn):
        old_data = code.dict()
        return PostalCodeOut(id=id, **old_data)

    def record_to_code_out(self, record):
        return PostalCodeOut(
            id=record[0], postal_code=record[1], neighborhood_id=record[2]
        )

    def get_all(self) -> Union[List[PostalCodeOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, postal_code, neighborhood_id
                        FROM postal_codes
                        """
                    )
                    return [
                        self.record_to_code_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve all postal codes."}

    def get_one(self, postal_code_id: int) -> Optional[PostalCodeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , postal_code
                            , neighborhood_id
                        FROM postal_codes
                        WHERE id = %s
                        """,
                        [postal_code_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_code_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not find postal code."}

    def create(self, code: PostalCodeIn) -> PostalCodeOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO postal_codes (postal_code, neighborhood_id)
                        VALUES (%s, %s)
                        RETURNING id;
                        """,
                        [code.postal_code, code.neighborhood_id],
                    )
                    id = result.fetchone()[0]
                    return self.code_in_to_out(id, code)
        except Exception as e:
            print(e)
            return {"message": "Could not create postal code."}

    def update(
        self, postal_code_id: int, code: PostalCodeIn
    ) -> Union[PostalCodeOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE postal_codes
                        SET postal_code = %s
                        , neighborhood_id = %s
                        WHERE id = %s
                        """,
                        (
                            code.postal_code,
                            code.neighborhood_id,
                            postal_code_id,
                        ),
                    )
                    return self.code_in_to_out(postal_code_id, code)
        except Exception as e:
            print(e)
            return {"message": "Could not update postal code."}

    def delete(self, postal_code_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM postal_codes
                        WHERE id = %s
                        """,
                        [postal_code_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False
