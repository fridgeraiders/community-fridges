import React, { useEffect, useState } from "react";
import useAToken from "./UIAuth.tsx";
import { fetchData } from "./utils/fetchdata.js";

export default function FridgeCheckInsList({ convertDateTimeFormat }) {
  const [FridgeCheckins, setFridgeCheckIns] = useState([]);
  const [fridgeList, setFridgeList] = useState([]);
  const [itemEntries, setItemEntries] = useState([]);
  const [foodStatuses, setFoodStatuses] = useState([]);
  const { fetchWithCookie } = useAToken();
  const { token } = useAToken();
  const title = "Fridge Check-Ins";
  const [isLoading, setIsLoading] = useState(true); // Initialize loading state

  async function getFridgeCheckInsList() {
    const url = `${process.env.REACT_APP_API_HOST}/api/fridgecheckins/`;
    const response = await fetchWithCookie(url);
    const status = response[1];
    if (status) {
      const data = await response[0];
      setFridgeCheckIns(data);
      setIsLoading(false); // set loading state to false when data is fetched
    } else {
      console.error(response);
    }
  }

  const getItemDetails = (checkinItems) => {
    const names = [];
    if (FridgeCheckins.length > 0) {
      for (let good of checkinItems) {
        const result = itemEntries.find((item) => item.id === good);
        names.push(result.name);
      }
    }
    return names;
  };

  const getFridgeAddress = (checkinFridgeID) => {
    const fridge = fridgeList.find((f) => f.id === checkinFridgeID);
    return fridge.location;
  };

  const renderStars = (clean_status) => {
    const totalStars = 3;
    const filledStars = Math.min(clean_status, totalStars);

    const stars = [];
    for (let i = 0; i < totalStars; i++) {
      stars.push(
        <svg
          key={i}
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          fill={i < filledStars ? "currentColor" : "none"}
          className="w-6 h-6 text-yellow-500"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M11.48 3.499a.562.562 0 011.04 0l2.125 5.111a.563.563 0 00.475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 00-.182.557l1.285 5.385a.562.562 0 01-.84.61l-4.725-2.885a.563.563 0 00-.586 0L6.982 20.54a.562.562 0 01-.84-.61l1.285-5.386a.562.562 0 00-.182-.557l-4.204-3.602a.563.563 0 01.321-.988l5.518-.442a.563.563 0 00.475-.345L11.48 3.5z"
          />
        </svg>
      );
    }

    return (
      <p className="inline-flex items-center justify-center px-2.5 py-0.5 text-white">
        {stars}{" "}
      </p>
    );
  };

  useEffect(() => {
    getFridgeCheckInsList();
    fetchData("/fridges", setFridgeList);
    fetchData("/food_statuses", setFoodStatuses);
    fetchData("/items/", setItemEntries);
    document.title = title;
    // eslint-disable-next-line
  }, []);

  if (token) {
    if (isLoading) {
      return <div>Loading Check-ins</div>;
    } else {
      return (
        <>
          <section className="container px-4">
            <div className="flex items-center gap-4">
              <h1 className="mt-6 mb-6 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
                <span className="text-transparent bg-clip-text bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500">
                  Fridge Check-ins
                </span>
              </h1>
              <span className="px-3 py-1 text-xs text-orange-600 bg-orange-100 rounded-full dark:bg-gray-800 dark:text-blue-400">
                {FridgeCheckins.length}
              </span>
            </div>
            <div className="flex flex-col">
              <div className="mx-4 sm:-mx-6 lg:-mx-8">
                <div className="inline-flex py-2 align-middle md:px-6 lg:px-8">
                  <div className="overflow-hidden border border-gray-300 dark:border-gray-300 md:rounded-lg">
                    <table className="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
                      <thead className="bg-white dark:bg-black-800">
                        <tr key="tablehead">
                          <th className="px-2 sm:px-4 lg:px-12 py-1.5 sm:py-2.5 lg:py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                            Fridge Name
                          </th>
                          <th className="px-2 sm:px-4 lg:px-12 py-1.5 sm:py-2.5 lg:py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                            Date
                          </th>
                          <th
                            scope="col"
                            className="px-2 sm:px-4 lg:px-12 py-1.5 sm:py-2.5 lg:py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                          >
                            Food Status
                          </th>

                          <th className="px-2 sm:px-4 lg:px-12 py-1.5 sm:py-2.5 lg:py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                            Clean Status
                          </th>

                          <th className="px-2 sm:px-4 lg:px-12 py-1.5 sm:py-2.5 lg:py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                            In Stock Items
                          </th>

                          <th className="px-2 sm:px-4 lg:px-12 py-1.5 sm:py-2.5 lg:py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                            Photo (optional)
                          </th>

                          <th className="px-2 sm:px-4 lg:px-12 py-1.5 sm:py-2.5 lg:py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                            Additional Details
                          </th>

                          <th className="px-2 sm:px-4 lg:px-12 py-1.5 sm:py-2.5 lg:py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                            Visitor
                          </th>
                        </tr>
                      </thead>
                      <tbody className="bg-white divide-y divide-gray-200 dark:divide-gray-200 dark:bg-gray-200">
                        {FridgeCheckins.map((fridgeCheckIn) => {
                          return (
                            <tr
                              key={fridgeCheckIn.id}
                              className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
                            >
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="inline-flex items-center gap-x-3">
                                  <div className="flex items-center gap-x-2">
                                    <div>
                                      <h2 className="font-medium text-gray-800 dark:text-white ">
                                        {
                                          fridgeList[fridgeCheckIn.fridge - 1]
                                            .fridge_name
                                        }
                                      </h2>
                                      <p className="text-sm font-normal text-gray-600 dark:text-gray-400">
                                        {getFridgeAddress(fridgeCheckIn.fridge)}{" "}
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="inline-flex items-center gap-x-3">
                                  <time dateTime={`${fridgeCheckIn.date_time}`}>
                                    {convertDateTimeFormat(
                                      fridgeCheckIn.date_time
                                    )}
                                  </time>
                                </div>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="inline-flex items-center gap-x-3 capitalize">
                                  {
                                    foodStatuses[fridgeCheckIn.food_status]
                                      .food_status_name
                                  }
                                </div>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="inline-flex items-center gap-1 p-4">
                                  {renderStars(fridgeCheckIn.clean_status)}
                                </div>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                                <div className="flex flex-wrap items-center gap-1 p-4">
                                  {getItemDetails(
                                    fridgeCheckIn.stocked_items
                                  ).map((cat, index) => {
                                    return (
                                      <React.Fragment key={`item` + index}>
                                        <span className="inline-flex items-center justify-center rounded-full bg-rose-600 px-2.5 py-0.5 text-white">
                                          {cat}
                                        </span>
                                        {(index + 1) % 5 === 0 ? <br /> : null}
                                      </React.Fragment>
                                    );
                                  })}
                                </div>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-wrap">
                                <img
                                  className="w-20 h-20 object-cover mx-auto"
                                  src={fridgeCheckIn.picture}
                                  alt={`${
                                    fridgeList[fridgeCheckIn.fridge - 1]
                                      .fridge_name
                                  } on ${convertDateTimeFormat(
                                    fridgeCheckIn.date_time
                                  )}`}
                                ></img>
                              </td>
                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-wrap">
                                <div className="inline-flex items-center gap-x-3">
                                  {fridgeCheckIn.additional}
                                </div>
                              </td>

                              <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-wrap">
                                <div className="inline-flex items-center gap-x-3 capitalize">
                                  {fridgeCheckIn.visitor}
                                </div>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
      );
    }
  } else {
    return (
      <div
        role="alert"
        className="rounded border-s-4 border-red-500 bg-red-50 p-4"
      >
        <div className="flex items-center gap-2 text-red-800">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="h-5 w-5"
          >
            <path
              fillRule="evenodd"
              d="M9.401 3.003c1.155-2 4.043-2 5.197 0l7.355 12.748c1.154 2-.29 4.5-2.599 4.5H4.645c-2.309 0-3.752-2.5-2.598-4.5L9.4 3.003zM12 8.25a.75.75 0 01.75.75v3.75a.75.75 0 01-1.5 0V9a.75.75 0 01.75-.75zm0 8.25a.75.75 0 100-1.5.75.75 0 000 1.5z"
              clipRule="evenodd"
            />
          </svg>
          <strong className="block font-medium">
            {" "}
            Sorry ! Access not authorized.{" "}
          </strong>
        </div>

        <p className="mt-2 text-sm text-red-700">
          You need to Login to see this page T_T
        </p>
      </div>
    );
  }
}
