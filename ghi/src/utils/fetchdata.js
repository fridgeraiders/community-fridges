export const fetchData = async (endpoint, statemanager) => {
  const url = `${process.env.REACT_APP_API_HOST}${endpoint}`;
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    statemanager(data);
  } else {
    console.error(response);
  }
};
