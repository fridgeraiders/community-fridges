export const handleChecked = (e, callback, state) => {
  if (e.target.checked) {
    callback([...state, e.target.value]);
  } else {
    const newArr = state.filter((item) => item !== e.target.value);
    callback(newArr);
  }
};
