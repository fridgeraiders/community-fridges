export const handleChange = (event, callback) => {
  const { target } = event;
  const { value } = target;
  callback(value);
};
