import { React, useEffect, useState } from "react";
import { AuthProvider } from "./UIAuth.tsx";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import "./index.css";

import DesktopNav from "./Nav.js";
import MobileNav from "./MobileNav.js";
import Main from "./Main.js";
import SignupForm from "./SignupForm.js";
import LoginForm from "./LoginForm.js";
import UsersList from "./UsersList.js";
import NewResourceChecklist from "./NewResourceChecklist.js";

import FridgeList from "./FridgeList.js";
import CreateFridge from "./CreateFridge.js";
import FridgeCheckin from "./fridgecheckin.js";
import FridgeCheckInsList from "./FridgeCheckIns.js";
import FridgeRequest from "./fridgerequest.js";
import FridgeRequests from "./fridgeRequestList.js";

import ListPantries from "./ListPantries.js";
import PantryDetail from "./PantryDetail.js";
import AddNewPantry from "./AddNewPantry.js";
import FridgeDetail from "./FridgeDetail.js";
import NotFound from "./NotFound.js";
import PantryCheckInForm from "./PantryCheckInForm.js";
import PantryCheckInsList from "./PantryCheckIns.js";
import PantryRequestForm from "./PantryRequestForm.js";
import PantryRequestList from "./PantryRequestList.js";
import NeighborhoodList from "./NeighborhoodList.js";
import NeighborhoodForm from "./NeighborhoodForm.js";


function App(props) {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  const [neighborhoodsList, setNeighborhoodsList] = useState([]);

  // Helper Functions
  const convertTimeFormat = (time) => {
    const [hours, minutes] = time.split(":");
    const period = +hours < 12 ? "AM" : "PM";
    const hoursAdjusted =
      +hours > 12 ? hours - 12 : hours === "00" ? 12 : +hours;
    return `${hoursAdjusted}:${minutes} ${period}`;
  };

  const convertDateTimeFormat = (date_time) => {
    const dateObject = new Date(date_time);
    return dateObject.toLocaleString();
  };

  async function getNeighborhoodsList() {
    const url = `${process.env.REACT_APP_API_HOST}/neighborhoods`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setNeighborhoodsList(data);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    getNeighborhoodsList();
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      <BrowserRouter basename={basename}>
        <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
          <DesktopNav />
          <MobileNav />
          <Routes>
            {/* ACCOUNTS */}
            <Route exact path="" element={<Main />}></Route>
            <Route path="*" element={<NotFound />} />
            <Route exact path="/signup" element={<SignupForm />}></Route>
            <Route exact path="/login" element={<LoginForm />}></Route>
            <Route
              path="/newresourcechecklist"
              element={<NewResourceChecklist />}
            ></Route>
            <Route path="/users" element={<UsersList />}></Route>
            {/* FRIDGES */}
            <Route
              exact
              path="/fridges/"
              element={<FridgeList convertTimeFormat={convertTimeFormat} />}
            ></Route>
            <Route
              exact
              path="/fridges/new"
              element={<CreateFridge neighborhoodsList={neighborhoodsList} />}
            ></Route>
            <Route
              exact
              path="/fridgecheckins/new"
              element={<FridgeCheckin />}
            />
            <Route
              path="/fridgecheckins/"
              element={
                <FridgeCheckInsList
                  convertDateTimeFormat={convertDateTimeFormat}
                />
              }
            ></Route>
            <Route path="/fridgerequests/" element={<FridgeRequests />}></Route>
            <Route
              exact
              path="/fridgerequests/new/"
              element={<FridgeRequest />}
            />
            {/* PANTRIES */}
            <Route
              exact
              path="/pantries/"
              element={<ListPantries convertTimeFormat={convertTimeFormat} />}
            ></Route>
            <Route path="/pantries/new" element={<AddNewPantry />}></Route>
            <Route
              path="/pantries/:id"
              element={
                <PantryDetail
                  convertTimeFormat={convertTimeFormat}
                  convertDateTimeFormat={convertDateTimeFormat}
                />
              }
            ></Route>
            <Route
              path="/pantrycheckins/new"
              element={<PantryCheckInForm />}
            ></Route>
            <Route
              path="/pantrycheckins/"
              element={
                <PantryCheckInsList
                  convertDateTimeFormat={convertDateTimeFormat}
                />
              }
            ></Route>
            <Route
              path="/pantryrequests/"
              element={<PantryRequestList />}
            ></Route>
            <Route
              path="/pantryrequests/new"
              element={<PantryRequestForm />}
            ></Route>
            {/* NEIGHBORHOODS */}
            <Route path="/neighborhoods" element={<NeighborhoodList />}></Route>
            <Route
              exact
              path="/neighborhoods/new"
              element={<NeighborhoodForm />}
            ></Route>
            <Route
              path="/fridges/:id/"
              element={
                <FridgeDetail
                  convertTimeFormat={convertTimeFormat}
                  convertDateTimeFormat={convertDateTimeFormat}
                />
              }
            ></Route>
          </Routes>
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
